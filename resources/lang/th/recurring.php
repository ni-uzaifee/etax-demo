<?php

return [
    'index' => [
        'recipient'                 =>  'ผู้รับ',
        'recipient_card'            =>  'บัตรผู้รับ',
        'company_name'              =>  'ชื่อบริษัท',
        'customer_code'             =>  'รหัสอ้างอิง',
        'daterange'                 =>  'วันที่',
        'search'                    =>  'ค้นหา',
        'search_result'             =>  'การค้นหา',
        'place_holder_customer'     =>  'รหัสลูกค้า',
        'export_card'               =>  'ดาวน์โหลด',
        'remove'                    =>  'ถูกลบไปแล้ว',
        'remove_confirm'            =>  'ลบข้อมูลบัตร?',
        'remove_detail'             =>  'คุณต้องการที่จะลบข้อมูลบัตรหรือไม่?',
        'place_daterange'           =>  'เริ่มวันที่ - ถึงวันที่',
        'delete_all'                =>  'ลบข้อมูลบัตรทั้งหมด'
    ],
];
