<?php
return 
    [
        'app'                         => 'แอป',
        'app_name'                    => 'ชื่อแอป',
        'function'                    => 'ฟังก์ชั่น',
        'function_management'         => 'จัดการฟังก์ชั่น',
        'function_name'               => 'ชื่อฟังก์ชั่น',
        'function_type'               => 'ประเภทฟังก์ชั่น',
        'function_description'        => 'คำอธิบายฟังก์ชั่น',
        'search_app'                  => 'ค้นหาแอป',
        'search_function_name'        => 'ค้นหาชื่อฟังก์ชั่น',
        'search_function_type'        => 'ค้นหาประเภทฟังก์ชั่น',
        'agent'                       => 'Agent',
        'corporate'                   => 'Corporate',
        'select_app_type'             => 'กรุณาเลือกประเภทแอป',
        'select_function_type'        => 'กรุณาเลือกประเภทฟังก์ชั่น',
    ];