<?php
return 
    [
        'download'      => [
            'title'             => 'รายงานประจำวัน',
            'corporate'         => 'ค้นหาบริษัท',
            'search'            => 'ชื่อบริษัท',
            'search_help'       => 'ค้นหาด้วยชื่อบริษัท กรุณาใส่ตัวอักษรอย่างน้อย 3 ตัว',
            'select_corporate'  => 'เลือกบริษัทในตัวเลือกข้างใต้',
            'report_list'       => 'รายงาน',
        ],
        'inquiry_page'    => [
            'title'                         => 'ค้นหารายงาน',
            'report_list'                   => 'ผลการค้นหา',
            'search'                        => 'ค้นหา',
            'select_report_type'            => 'โปรดเลือกประเภทรายงาน',
            'select_date_range'             => 'โปรดเลือกวันเวลา',
            'select_start_month'            => 'โปรดเลือกเดือนเริ่มต้น',
            'select_end_month'              => 'โปรดเลือกเดือนสุดท้าย',
            'corporate_report'              => 'รายงานบริษัท',
            'bill_payment_report'           => 'รายงานใบแจ้งหนี้',
            'payment_transaction_report'    => 'รายงานการชำระเงิน',
            'payment_reconcile_report'      => 'รายงานตรวจสอบการชำระเงิน',
            'auditlog_report'               => 'รายงานบันทึกการตรวจสอบ',
            'monthly_payment_transaction'   => 'รายการการชำระเงินประจำเดือน',
            'monthly_corporate_usage'       => 'รายการการใช้งานของบริษัทประจำเดือน',
            'monthly_bill_payment_usage'    => 'รายการใบแจ้งหนี้ประจำเดือน',
        ],
    ];