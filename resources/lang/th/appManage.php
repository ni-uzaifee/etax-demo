<?php
return 
    [
       'title' => 'แอพพลิเคชัน',
        'create' => 'สร้างแอพพลิเคชัน',
        'search_app' => 'ค้นหาข้อมูลแอพพลิเคชัน',
        'all_status' => 'สถานะทั้งหมด',
        'active' => 'ใช้งาน',
        'inactive' => 'ไม่ใช้งาน',
        'view' => 'ดูข้อมูล', 

    ];