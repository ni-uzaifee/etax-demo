<?php
return 
    [
        'login'  => 'เข้าสู่ระบบ',
        'staff_name'  => 'อีเมลพนักงาน',
        'password'  => 'รหัสผ่าน',
        'signin'  => 'เข้าสู่ระบบ',
        'copyright'  => 'COPYRIGHT 2015 DIGIO (THAILAND) CO.,LTD. ALL RIGHTS RESERVED',
        'forgot'        => [
            'link'                  => 'ลืมรหัสผ่าน?',
            'title'                 => 'จดจำรหัสผ่านไม่ได้',
            'header'                => 'โปรดระบุอีเมลล์ที่เกี่ยวข้องกับผู้ใช้งานของคุณ.',
            'placeholder'           => 'กรอกอีเมลล์',
        ],
    ];