<?php

return [
    'index' => [
        'recipient'                 =>  'Recipient',
        'recipient_card'            =>  'Recipient Card Store',
        'company_name'              =>  'Company Name',
        'customer_code'             =>  'Customer Code',
        'daterange'                 =>  'Date',
        'search'                    =>  'Search',
        'search_result'             =>  'Search Result',
        'place_holder_customer'     =>  'Recipient Code',
        'export_card'               =>  'Export',
        'remove'                    =>  'Removed',
        'remove_confirm'            =>  'Remove Card?',
        'remove_detail'             =>  'You want to remove cards?',
        'place_daterange'           =>  'Start date - End date',
        'delete_all'                =>  'Delete All'
    ],
];
