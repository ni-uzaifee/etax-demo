<?php
return 
    [
        'app'                         => 'App',
        'app_name'                    => 'App Name',
        'function'                    => 'Function',
        'function_management'         => 'Function Management',
        'function_name'               => 'Function Name',
        'function_type'               => 'Function Type',
        'function_description'        => 'Function Description',
        'search_app'                  => 'Search App',
        'search_function_name'        => 'Search Function Name',
        'search_function_type'        => 'Search Function Type',
        'agent'                       => 'Agent',
        'corporate'                   => 'Corporate',
        'select_app_type'             => 'Please Select App Type',
        'select_function_type'        => 'Please Select Function Type',
    ];