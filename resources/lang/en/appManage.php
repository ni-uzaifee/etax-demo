<?php
return 
    [
        'title' => 'Apps',
        'create' => 'Create App',
        'search_app' => 'Search Apps',
        'all_status' => 'All status',
        'active' => 'Active',
        'inactive' => 'Inactive',
        'view' => 'View',

    ];