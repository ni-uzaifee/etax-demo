<?php
return 
    [
        'download'      => [
            'title'             => 'Daily Report',
            'corporate'         => 'Search for Corporates',
            'search'            => 'Corporate name',
            'search_help'       => 'Search terms must be at least 3 characters in length',
            'select_corporate'  => 'Please select a corporate from the list below',
            'report_list'       => 'Report list',
        ],
        'inquiry_page'    => [
            'title'                         => 'Inquiry Report',
            'report_list'                   => 'Inquiry results',
            'search'                        => 'Search',
            'select_report_type'            => 'Please select report types',
            'select_date_range'             => 'Please select date',
            'select_start_month'            => 'Please select start month',
            'select_end_month'              => 'Please select end month',
            'corporate_report'              => 'Corporate Report',
            'bill_payment_report'           => 'Bill Payment Report',
            'payment_transaction_report'    => 'Payment Transaction Report',
            'payment_reconcile_report'      => 'Payment Reconcile Report',
            'auditlog_report'               => 'Audit Log Report',
            'monthly_payment_transaction'   => 'Monthly Payment Transaction',
            'monthly_corporate_usage'       => 'Monthly Corporate Usage',
            'monthly_bill_payment_usage'    => 'Monthly Bill Payment Usage',
        ],
    ];