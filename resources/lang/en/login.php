<?php
return 
    [
        'login'  => 'Login',
        'staff_name'  => 'Staff name of Email',
        'password'  => 'Password',
        'signin'  => 'SIGN IN',
        'copyright'  => 'COPYRIGHT 2015 DIGIO (THAILAND) CO.,LTD. ALL RIGHTS RESERVED',
        'forgot'        => [
            'link'                  => 'forgot password?',
            'title'                 => 'Forgot Password ?',
            'header'                => 'Enter the email associated with your account.',
            'placeholder'           => 'Enter Email Address',
        ]
    ];