<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<style> 

th{
	background-color: #B2D2DC;
}
</style>
<body>

	<table>
		<thead>
			<tr>
				<th> firstname_th </th>
				<th> surname_th </th>
				<th> firstname_en </th>
				<th> surname_en </th>
				<th> citizen_id </th>
				<th> email </th>
				<th> telephone </th>
				<th> address_th </th>
				<th> address_en </th>
				<th> reference_1 </th>
				<th> reference_2 </th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td> สมชาย </td>
				<td> ใจกล้า </td>
				<td> somchai </td>
				<td> jaikra </td>
				<td> 1234567890123 </td>
				<td> kk@kk.com </td>
				<td> 029991150 </td>
				<td> เลขที่ 57 อาคารปาร์คเวนเชอร์ อีโคเพล็กซ์ ชั้น 77 ถนนวิทยุ แขวงลุมพินี เขตปทุมวัน, กรุงเทพฯ, ประเทศไทย </td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</tbody>
	</table>
</body>