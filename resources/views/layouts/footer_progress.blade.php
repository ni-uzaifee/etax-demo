<section id="main_footer_progress" class="progressbar m-t-75" style="margin-top: 70px;">
    <div class="d-flex flex-column">
        <div class="col-12">
            <div class="px-0">
                <div class="row mx-0">
                    <div id="body_progress" class="col-12 py-3">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>